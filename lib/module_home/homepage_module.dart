class Weather {
  String? cityName;
  double? temp;
  double? wind;
  int? humidity;
  int? pressure;
  double? feelslike;

  Weather(
      {this.cityName,
      this.temp,
      this.wind,
      this.humidity,
      this.pressure,
      this.feelslike});

// mfrud ykun bl response
  Weather.fromJson(Map<String, dynamic> json) {
    cityName = json["name"];
    temp = json["main"]["temp"];
    wind = json["wind"]["speed"];
    pressure = json["main"]["pressure"];
    humidity = json["main"]["himidity"];
    feelslike = json["main"]["feels_like"];
  }
}
