import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:weatherapp/module_home/homepage_module.dart';

class WeatherApiClient {
  Future<Weather>? getCurrentWeather(String? location) async {
    var endpoint = Uri.parse(
        "https://api.openweathermap.org/data/2.5/weather?q=$location&appid=b3ed336fc837b99d9625da1eb3dd8208&units=metric");
    var response = await http.get(endpoint);
    var body = jsonDecode(response.body);

    // ignore: avoid_print
    print(Weather.fromJson(body).cityName);
    return Weather.fromJson(body);
  }
}
