// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class CurrentWeather extends StatelessWidget {
  const CurrentWeather(this.icon, this.temp, this.location, {super.key});

  final IconData icon;
  final String temp;
  final String location;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          Icon(
            icon,
            color: Colors.orange,
            size: 64,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            temp,
            style: TextStyle(fontSize: 40),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            location,
            style: TextStyle(fontSize: 15, color: Color(0xFF5a5a5a)),
          )
        ],
      ),
    );
  }
}

class $ {}
