// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:weatherapp/module_home/homepage_module.dart';
import 'package:weatherapp/module_home/repository/weather_api_client.dart';
import 'package:weatherapp/module_home/ui/widgets/additional_information.dart';
import 'package:weatherapp/module_home/ui/widgets/current_weather.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  WeatherApiClient client = WeatherApiClient();
  Weather? data;

  Future<void> getData() async {
    data = await client.getCurrentWeather("Beirut");
  }

  @override
  void initState() {
    super.initState();
    client.getCurrentWeather("London");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf9f9f9),
      appBar: AppBar(
        backgroundColor: Color(0xFFf9f9f9),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        title: Text(
          'Weather App',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CurrentWeather(Icons.wb_sunny_rounded, "${data!.temp}",
                    "${data!.cityName}"),
                SizedBox(
                  height: 50,
                ),
                Text(
                  'Additional Information',
                  style: TextStyle(
                      fontSize: 24,
                      color: Color(0xdd212121),
                      fontWeight: FontWeight.bold),
                ),
                Divider(),
                SizedBox(
                  height: 50,
                ),
                AdditionalInformation("${data!.wind}", "${data!.humidity}",
                    "${data!.pressure}", "${data!.feelslike}")
              ],
            );
          } else if(snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          return Container();
        },
        future: getData(),
      ),
    );
  }
}
